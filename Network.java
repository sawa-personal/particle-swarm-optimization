//*******************************************************
// Network.java
// created by Sawada Tatsuki on 2017/12/02.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* ネットワークのクラス．ノード(Agent)とエッジ(Relation)を要素に持つ */

import java.util.*;

public class Network {
    public Agent agent[]; //ネットワークのノード
    public Relation[][] relation; //ネットワークのエッジ
    private double[][] distance; //ネットワークのエッジ
    private boolean[][] neighbor; //近傍関係を保存
    public Space space; //このネットワークを持つ空間
    public static int num = 30; //ネットワークのノード数
    
    public Network(Space space) {
	this.space = space; //このネットワークを持つ空間を設定
	/* start: エッジの初期設定 */
	this.neighbor = lBest(); //近傍情報．d = 2 でlbest, d = num でgbest．
	/* end: エッジの初期設定 */
	
	/* start: ノードの初期設定 */
	agent = new Agent[num];
	for(int i = 0; i < num; i++) {
	    agent[i] = new Agent(i, this); //ノードのインスタンス生成
	}
	/* end: ノードの初期設定 */
    }

    public void run(){
	for(int i = 0; i < num; i++) {
	    agent[i].observe(); //ノードに観測させる
	}
	for(int i = 0; i < num; i++) {
	    agent[i].move(); //ノードを規則に従って同時に動かす
	}
    }

    public boolean[] getNeighbors(int id) {
	boolean[] idNeighbor = new boolean[num];
	for(int i = 0; i < num - 1; i++) {
	    idNeighbor[i] = neighbor[id][i];
	}
	return idNeighbor;
    }

    private boolean[][] lBest() {
	boolean[][] neighbor = new boolean[num][num];
	
	for(int i = 0; i < num; i++) {
	    for(int j = 0; j < num; j++) {
		neighbor[i][j] = false;
	    }
	}
	neighbor[0][1] = true;
	neighbor[0][num - 1] = true;
	for(int i = 1; i < num - 1; i++) {
	    neighbor[i][i-1] = true;
	    neighbor[i][i+1] = true;
	    
	}
	neighbor[num - 1][num - 2] = true;
	neighbor[num - 1][0] = true;
	
	return neighbor;
    }
    
    private boolean[][] gBest() {
	boolean[][] neighbor = new boolean[num][num];
	for(int i = 0; i < num; i++) {
	    for(int j = 0; j < num; j++) {
		neighbor[i][j] = true;
	    }
	}
	return neighbor;
    }    
}
