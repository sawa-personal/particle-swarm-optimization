//*******************************************************
// Relation.java
// created by Sawada Tatsuki on 2017/12/02.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 関係のオブジェクト */

import java.util.*;

public class Relation {
    private double weight; //重み
    private boolean neighbor; //近傍か否か
    private int fst; //元のエージェントid
    private int sec; //先のエージェントid
    private Network network; //属するネットワーク

    public Relation(Network network){
	this.network = network; //属するネットワークを設定
    }

    public void setWeight(double weight){
	this.weight = weight;
    }
    
    public double getWeight(){
	return weight;
    }
}
