//*******************************************************
// Main.java
// created by Sawada Tatsuki on 2017/11/28.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* Processingにおける描画処理を行うメインクラス． */

import java.util.*;
import processing.core.*;

public class Main extends PApplet {
    public final int WIDTH = 800; //ウィンドウの横幅
    public final int HEIGHT = 600; //ウィンドウの縦幅
    public final int POINTRADIUS = 4; //粒子の半径
    private Vector agent[]; //粒子の座標
    private Vector[][] position; //粒子の座標
    private Vector foodSource; //餌の座標
    private int num; //粒子数
    
    public void settings() {
	size(WIDTH, HEIGHT); //ウィンドウサイズを指定
    }

    public void setup() {
	strokeWeight(0.5f); //線の太さを指定
	agent = Observer.initPosition(WIDTH, HEIGHT, POINTRADIUS); //初期座標を生成
	//position = Observer.initPosition(WIDTH, HEIGHT); //初期座標を生成
	foodSource = Observer.getFoodPosition(); //餌の位置を初期化
	//num = position.length; //粒子数を取得
	num = agent.length; //粒子数を取得
	//noStroke(); //点の枠線なし
    }
    
    public void draw() {
	Observer.run(); //系を動かす
	background(255); //背景色を指定．画面をリセットする役割もある．
	//position = Observer.getPosition(WIDTH, HEIGHT); //座標を取得
	agent = Observer.getPosition(WIDTH, HEIGHT, POINTRADIUS); //粒子の座標を取得
	fill(240, 240, 240);//粒子の色
	//粒子の数だけプロットを行う
	for(int i = 0; i < num; i++){
	    //triangle((float)position[i][0].getx(), (float)position[i][0].gety(), (float)position[i][1].getx(), (float)position[i][1].gety(), (float)position[i][2].getx(), (float)position[i][2].gety()); //順に頂点の(x,y)，左下の(x,y)，右下の(x,y)
	    ellipse((float)agent[i].getx(), (float)agent[i].gety(), 8, 8);
	}
	
	fill(255, 128, 0);
	foodSource = Observer.getFoodPosition(); //餌の位置を取得
	ellipse((float)foodSource.getx(), (float)foodSource.gety(), 12, 12);

    }
    
    public static void main(String args[]) {
	PApplet.main("Main"); //Mainクラスを呼び出してProcessingを起動
    }
}
