//*******************************************************
// Space.java
// created by Sawada Tatsuki on 2017/11/30.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* シミュレータのクラス．「環境」と「ネットワーク」を要素に持つ． */

import java.util.*;

public class Space {
    public static Environment environment; //環境
    public static Network network; //ネットワーク
    
    //コンストラクタ
    public Space() {
	environment = new Environment(); //環境の生成
	network = new Network(this); //ネットワークの生成
    }
    
    ///世界を単位時間だけ動かす
    public static void run(){
	network.run(); //ネットワークを動かす
	environment.run(); //環境を動かす
    }
}
