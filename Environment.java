//*******************************************************
// Environment.java
// created by Sawada Tatsuki on 2017/12/02.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 環境のオブジェクト．評価関数を持っている． */

import java.util.*;

public class Environment {
    public static final int DIM = 2; //空間次元
    public static final int WIDTH = 800; //空間の横幅
    public static final int HEIGHT = 600; //空間の縦幅
    private double counter = 0; //カウンター
    public Vector foodSource; //餌の位置

    public Environment() {
	//評価関数のパラメータの初期化
	foodSource = new Vector(100, 500); //餌の位置の初期値
    }

    //最小化問題用の評価関数．目的関数の値が小さいほど高く評価する．
    public double eval(Vector p) {
	double fval = function(p);
	if(fval >= 0) {
	    return 1 / (1 + fval);
	} else {
	    return 1 - fval;
	}
    }

    //目的関数．ここでは餌までの距離とする．
    private double function(Vector p) {
	return Benchmark.sphere(p.subtract(foodSource)); //Vector.distance(foodSource, p);
    }
    
    //環境を動かす
    public void run(){
	double deg = Math.PI / 180 * counter / 2;
	foodSource.setx(400 + 250 * Math.cos(deg));
	foodSource.sety(300 + 250 * Math.sin(deg));
	counter++;
	if(counter / 2 == 360) {
	    counter = 0;
	}
    }
}
