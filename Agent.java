//*******************************************************
// Agent.java
// created by Sawada Tatsuki on 2017/12/02.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* エージェントのオブジェクト */

import java.util.*;

public class Agent {
    
    public Vector p; //座標
    public Vector v; //速度
    private Vector pBest; //自分の最適位置(personal best)
    private double bestFitVal; //自分の適応度の最高記録
    private boolean[] neighbor; //近傍情報格納用(地理的近傍でなく社会的近傍とする)
    private double[] distance; //距離情報格納用

    private double W = 0.9; //慣性因子
    private double C1 = 1.5; //認知性学習因子
    private double C2 = 1.5; //社会性学習因子
    public static double VMAX = 3.0; //速さの最大値 (粒子が探索領域外に出難くするため)

    public int id; //エージェントのID
    public Network network; //属するネットワーク
    
    private Random rnd = new Random(); //乱数生成用インスタンス
    
    public Agent(int id, Network network) {
	this.id = id; //IDを設定
	this.network = network; //属するネットワークを設定
	p = new Vector(rnd.nextInt(network.space.environment.WIDTH), rnd.nextInt(network.space.environment.HEIGHT)); //座標
	double r = Math.random() * 2.0 * Math.PI; //ランダムな角度
	v = new Vector(VMAX * Math.random() * Math.cos(r), VMAX * Math.random() * Math.sin(r)); //速度
	bestFitVal = network.space.environment.eval(this.p); //自分の適応度
	pBest = p; //自分の位置を初期の最適位置をとする
	neighbor = network.getNeighbors(id); //隣接ノードの取得
	distance = new double[network.num]; //距離情報格納配列を用意
    }

    //観測する．
    public void observe() {
	//最大評価値を更新
	if(eval(p) > bestFitVal) {
	    bestFitVal = eval(p);
	    pBest = p;
	}
    }
    
    //エージェントを動かす
    public void move() {
	Agent bestNeighbor = getBestNeighbor();
	Vector gBest = bestNeighbor.p;
	Vector v0 = v.amp(W); //慣性成分
	Vector v1 = (pBest.subtract(p)).amp(C1 * Math.random()); //認知成分
	Vector v2 = (gBest.subtract(p)).amp(C2 * Math.random()); //社会成分
	v = Vector.constrict(v.add0(v0.add0(v1.add0(v2))), VMAX); //速度を更新	
	p.add(v.getx(), v.gety()); //vの方向に進む
	//regulate(); //エージェントが環境の領域外に出たときの補正
    }
    
    //適応度が最大の隣接ノードを返す
    private Agent getBestNeighbor() {
	double max = 0;
	int bestIndex = id;
	for(int i = 0; i < network.num; i++) {
	    if(neighbor[i] == false){
		continue;
	    }
	    if(eval(network.agent[i].p) >= max) {
		max = eval(network.agent[i].p);
		bestIndex = i;
	    }
	}
	return network.agent[bestIndex];
    }
    
    //エージェントが環境の領域外に出たとき跳ね返す
    private void regulate(){
	if(p.getx() > network.space.environment.WIDTH) {
	    p.setx(network.space.environment.WIDTH); //位置を領域の端に
	    v.setx(-v.getx()); //速度を逆方向に
	} else if(p.getx() < 0) {
	    p.setx(0); //位置を領域の端に
	    v.setx(-v.getx()); //速度を逆方向に
	}
	if(p.gety() > network.space.environment.HEIGHT) {
	    p.sety(network.space.environment.HEIGHT); //位置を領域の端に
	    v.sety(-v.gety()); //速度を逆方向に
	} else if(p.gety() < 0) {
	    p.sety(0); //位置を領域の端に
	    v.sety(-v.gety()); //速度を逆方向に	    
	}    ; //距離情報格納配列を用意
    }

    private double eval(Vector v){
	return network.space.environment.eval(v);
    }
}
