//*******************************************************
// Benchmark.java
// created by Sawada Tatsuki on 2017/12/17.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 最適化アルゴリズムのためのベンチマーク関数 */

import java.util.*;

public class Benchmark {
    public static double sphere(Vector p){
	return p.getx() * p.getx() + p.gety() * p.gety();
    }
}
